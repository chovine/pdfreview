use super::pixmap::pdffile_to_pixmap;
use cairo::{Context, Format, ImageSurface};
use mupdf::{page::Page, Pixmap};
use std::cmp::{max, min};

fn pixmap_to_surface(
    pixmap: &Pixmap,
    scale: f32,
    focus_x: f32,
    focus_y: f32,
    output_width: i32,
    output_height: i32,
) -> ImageSurface {
    let first_x = max(((scale - 1.0) * focus_x) as i32, 0);
    let first_y = max(((scale - 1.0) * focus_y) as i32, 0);

    let surface_w = min(output_width, pixmap.width() as i32 - first_x as i32) as i32;
    let surface_h = min(output_height, pixmap.height() as i32 - first_y as i32) as i32;

    let input_w = pixmap.width() as i32;
    let input_h = pixmap.height() as i32;

    let mut surface = ImageSurface::create(Format::Rgb24, surface_w, surface_h).unwrap();

    let last_x = min(surface_w + first_x, input_w);
    let last_y = min(surface_h + first_y, input_h);

    {
        let mut rgb_data = surface.data().unwrap();
        let mut rgb_data_iter = rgb_data.iter_mut();
        let mut pixmap_iter = pixmap.samples().iter();

        let (mut curr_x, mut curr_y) = (0, 0);

        'top: loop {
            if curr_x < first_x {
                let skip = first_x - curr_x;

                pixmap_iter.nth(skip as usize * 3 - 1);
                curr_x += skip;
            }

            if curr_y < first_y {
                let skip = first_y - curr_y;
                pixmap_iter.nth((skip * input_w * 3) as usize - 1);
                curr_y += skip;
            }

            if curr_y > last_y {
                let skip = first_y + input_h - curr_y;

                pixmap_iter.nth((skip * input_w * 3) as usize - 1);
                curr_y = first_y;
            }

            if curr_x == last_x {
                let skip = first_x + input_w - curr_x;

                pixmap_iter.nth(skip as usize * 3 - 1);
                curr_x = first_x;
                curr_y += 1;
            }

            let ins = get_next_pixel(&mut pixmap_iter, false);

            curr_x += 1;
            if curr_x >= input_w {
                curr_x = 0;
                curr_y += 1;
            }

            let mut outs = get_next_pixel_mut(&mut rgb_data_iter, true);

            for i in 0..4 {
                if outs[i].is_none() {
                    log::debug!("Reached end of drawing area");
                    break 'top;
                }
            }

            let b = outs[0].take().unwrap();
            let g = outs[1].take().unwrap();
            let r = outs[2].take().unwrap();
            let a = outs[3].take().unwrap();

            for i in 0..4 {
                if (ins[i].is_none()) & (i != 3) {
                    log::debug!("Reached end of input area");
                    break 'top;
                }
            }

            // Correct endianess...
            *r = ins[0].unwrap().to_owned();
            *g = ins[1].unwrap().to_owned();
            *b = ins[2].unwrap().to_owned();
            *a = 0;
        }
    }

    surface
}

fn get_next_pixel_mut<'a>(
    byte_iter: &mut dyn Iterator<Item = &'a mut u8>,
    alpha: bool,
) -> [Option<&'a mut u8>; 4] {
    let mut pixel_options: [Option<&'a mut u8>; 4] = [None, None, None, None];

    for i in 0..4 {
        if !alpha & (i == 3) {
            break;
        }

        pixel_options[i] = byte_iter.next();
    }

    return pixel_options;
}

fn get_next_pixel<'a>(
    byte_iter: &mut dyn Iterator<Item = &'a u8>,
    alpha: bool,
) -> [Option<&'a u8>; 4] {
    let mut pixel_options: [Option<&'a u8>; 4] = [None; 4];
 
    for i in 0..4 {
        if !alpha & (i == 3) {
            break;
        }

        pixel_options[i] = byte_iter.next();
    }

    return pixel_options;
}

pub fn render_page(
    page: &Page,
    context: &Context,
    scale: f32,
    focus_x: f32,
    focus_y: f32,
    output_width: i32,
    output_height: i32,
) {
    let pixmap = pdffile_to_pixmap(page, scale, scale);
    let surface = pixmap_to_surface(
        &pixmap,
        scale,
        focus_x,
        focus_y,
        output_width,
        output_height,
    );

    context.set_source_surface(&surface, 0.0, 0.0).unwrap();
    context.paint().unwrap();
}
