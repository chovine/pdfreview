pub fn get_bit(index: usize, x: u32) -> bool {
	let mask =  1 << index;
	x & mask > 0
}

pub fn set_bit(index: usize, x: u32, value: bool) -> u32 {
	let mask =  1 << index;
	let mut y = x;
	if value {
		y = y | mask;
	} else {
		y = y & (!mask);
	}
	y
}