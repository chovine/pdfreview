use super::render::render_page;
use super::state::SharedState;
use gtk::prelude::*;
use gtk::{ApplicationWindow, DrawingArea, EventControllerKey, Inhibit, EventControllerLegacy, EventControllerMotion, Box, ListBox, Label, TextView, TextBuffer, Separator, ScrolledWindow, CenterBox};
use gdk::EventType;
use super::pdfobjects::get_annotations;
use std::collections::HashMap;
use std::sync::Mutex;


const SCALE_MAX: f32 = 10.0;

pub fn init_ui(root_window: &ApplicationWindow, global_state: SharedState) {
    let drawing_area = init_render_area(&global_state);
    // let listcont = Box::builder()
    //     .orientation(gtk::Orientation::Vertical)
    //     .spacing(0)
    //     .width_request(350)
    //     .homogeneous(false)
    //     .build();

	let listbox = ListBox::builder()
        //.orientation(gtk::Orientation::Vertical)
        //.spacing(0)
        .width_request(350)
        //.homogeneous(false)
        .build();


	//listcont.append(&listbox);

    let scrolledwindow = ScrolledWindow::builder()
        .child(&listbox)
        .propagate_natural_width(true)
        .vexpand(true)
        .has_frame(true)
        .build();


	//let mut annot_row_dict = HashMap::new();
    {
		let page_nr =  global_state.get().page_nr;
		let local_state = global_state.get();
		let doc = local_state.document.lock().unwrap();
		let page = doc.load_page(page_nr).unwrap();
		let annots = get_annotations(page);

		for mut a in annots {
			
			a.set_visible(false).unwrap();
			if a.contents.is_none() {
				continue;
			}

			let date = a.date.unwrap().to_string();
			let entry = init_list_entry(&date, &a.contents.unwrap().clone());
			a.contents = None;

			let m = Mutex::new(a);

			let evc = EventControllerLegacy::new();
			let dr = drawing_area.clone();
			evc.connect_event(move |_, e| {
				if e.event_type() == EventType::ButtonPress {

					m.lock().unwrap().toggle_visible().unwrap();
					dr.queue_draw();
				}
				Inhibit(false)
			}); 

			entry.add_controller(&evc);
			listbox.append(&entry);
			//annot_row_dict.insert(listbox.row_at_index(i).unwrap(), a);
		}
	}


	// listbox.connect_row_activated(move |_,r| {
	// 	let mut a = annot_row_dict.get_mut(r).unwrap();
	// 	a.set_visible(true).unwrap();
	// });

    let cbox = Box::new(gtk::Orientation::Horizontal, 0);

    cbox.append(&drawing_area);
    cbox.append(&scrolledwindow);

    root_window.set_child(Some(&cbox));

    init_event_listeners(root_window, global_state, drawing_area);
}


fn init_list_entry(label: &str, content: &str) -> Box {
    let cbox = Box::builder()
        .orientation(gtk::Orientation::Vertical)
        .spacing(0)
        .homogeneous(false)
        .build();
    
    let label = Label::new(Some(label));
    let separator = Separator::new(gtk::Orientation::Horizontal);
    let buffer = TextBuffer::builder()
            .text(content)
            .build();

    
    
    let textview = TextView::builder()
        .buffer(&buffer)
        .wrap_mode(gtk::WrapMode::WordChar)
        .margin_start(0)
        .margin_end(0)
        .margin_bottom(0)
        .margin_top(0)
        .top_margin(10)
        .bottom_margin(10)
        .left_margin(10)
        .right_margin(10)
        .editable(false)
        .cursor_visible(false)
        .build();

        // let scrolledwindow = ScrolledWindow::builder()
        // .child(&textview)
        // //.max_content_height(500)
        // .vscrollbar_policy(gtk::PolicyType::Never)
        // .build();

    cbox.append(&label);
    cbox.append(&separator);
    cbox.append(&textview);

    return cbox



}

fn init_render_area(global_state: &SharedState) -> DrawingArea {
    let drawing_area = DrawingArea::builder()
    .vexpand(true)
    .hexpand(true)
    .build();

    let local_state = global_state.clone();
    drawing_area.set_draw_func(move |_, context, width, height| {
        log::debug!("Redrawing");


		let local_state = local_state.get();
		let scale = local_state.scale;
		let pointer_x = local_state.pointer_location.0;
		let pointer_y = local_state.pointer_location.1;
		let page_nr = local_state.page_nr;

		let doc = local_state.document.lock().unwrap();
        let page = doc.load_page(page_nr).unwrap();

        render_page(&page, context, scale, pointer_x, pointer_y, width, height);
    });

    drawing_area
}

fn init_event_listeners(
    root_window: &ApplicationWindow,
    global_state: SharedState,
    drawing_area: DrawingArea,
) {
    let evc_k = EventControllerKey::new();
    let local_state = global_state.clone();

    evc_k.connect_key_pressed(move |_, key, _, _| {
        let mut local_state = local_state.get();
        match key.name().unwrap().as_str() {
            "p" => {
                (*local_state).scale = ((*local_state).scale * 1.1).min(SCALE_MAX);
                drawing_area.queue_draw();
            }
            "m" => {
                (*local_state).scale *= 0.9;
                drawing_area.queue_draw();
            }
            _ => (),
        }

        log::info!("Set scale {}", local_state.scale);
        Inhibit(false)
    });

    let evc_m = EventControllerMotion::new();
    let local_state = global_state.clone();

    evc_m.connect_motion(move |_, x, y| {
        let mut local_state = local_state.get();
        (*local_state).pointer_location = (x as f32, y as f32);
    });

    root_window.add_controller(&evc_k);
    root_window.add_controller(&evc_m);
}
