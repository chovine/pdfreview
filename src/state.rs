use mupdf::pdf::PdfDocument;
use std::sync::{Arc, Mutex, MutexGuard};

#[derive(Clone)]
pub struct State {
    pub scale: f32,
    pub pointer_location: (f32, f32),
    pub page_nr: i32,
    pub document: Arc<Mutex<PdfDocument>>,
}

pub struct SharedState {
    inner: Arc<Mutex<State>>,
}

impl SharedState {
    pub fn new(state: State) -> Self {
        Self {
            inner: Arc::new(Mutex::new(state)),
        }
    }

    pub fn clone(&self) -> Self {
        Self {
            inner: self.inner.clone(),
        }
    }

    pub fn get(&self) -> MutexGuard<State> {
        self.inner.lock().unwrap()
    }

	// pub fn get_document(&self) -> MutexGuard<PdfDocument> {
    //     self.get().document.lock().unwrap()
    // }
}
