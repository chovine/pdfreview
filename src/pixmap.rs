use mupdf::colorspace::Colorspace;
use mupdf::matrix::Matrix;
use mupdf::page::Page;
use mupdf::pixmap::Pixmap;

pub fn pdffile_to_pixmap(page: &Page, scale_x: f32, scale_y: f32) -> Pixmap {
    // Convert to pixmap
    let transform = Matrix::new_scale(scale_x, scale_y);

    let colorspace = Colorspace::device_rgb();

    page.to_pixmap(&transform, &colorspace, 00.0, true)
        .unwrap()
}
