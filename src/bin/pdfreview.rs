use gtk::prelude::*;
use gtk::{Application, ApplicationWindow};
use mupdf::pdf::PdfDocument;
use pdfreview::state::{SharedState, State};
use pdfreview::ui::init_ui;
use std::sync::{Arc, Mutex};

const FILENAME: &str = "test.pdf";

fn main() {
    simple_logger::init().unwrap();

    let app = Application::builder()
        .application_id("org.gtk.pdfreview")
        .build();

    app.connect_activate(|app| {
        let window = ApplicationWindow::builder()
            .application(app)
            .title("PDFReview")
            .build();

        let doc = PdfDocument::open(FILENAME).unwrap();

        let global_state = SharedState::new(State {
            scale: 1.0,
            pointer_location: (0.0, 0.0),
            page_nr: 12,
            document: Arc::new(Mutex::new(doc)),
        });

        init_ui(&window, global_state);

        window.present();
    });

    app.run();
}
