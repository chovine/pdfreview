use mupdf::error::Error;
use mupdf::pdf::{PdfObject, PdfPage};
use mupdf::rect::Rect;
use std::collections::HashMap;
use mupdf::page::Page;
use chrono::prelude::*;
use super::bit::*;
use mupdf::pdf::PdfDocument;


#[derive(Debug)]
pub struct Array {
    inner: Vec<PdfObject>,
}

impl Array {
    pub fn from(object: &PdfObject) -> Result<Self, Error> {
        if !object.is_array()? {
            return Err(Error::InvalidPdfDocument);
        }

        let mut array = Vec::new();

        let l = object.len()?;
        for i in 0..l {
            let value = object.get_array(i as i32)?;
            if value.is_some() {
                array.push(value.unwrap());
            }
        }

        Ok(Self { 
			inner: array,
		})
    }

	// pub fn delete(&mut self, index: i32) -> Result<(), Error> {
	// 	self.inner.remove(index as usize);
	// 	self.array_ref.array_delete(index)?;
	// 	Ok(())
	// }

    pub fn into_vec(self) -> Vec<PdfObject> {
        self.inner
    }
}

#[derive(Debug)]
pub struct Dict {
    inner: HashMap<String, PdfObject>,
}

impl Dict {
    pub fn from(object: PdfObject) -> Result<Self, Error> {
        if !object.is_dict()? {
            return Err(Error::InvalidPdfDocument);
        }

        let mut map = HashMap::new();

        let l = object.dict_len()?;
        for i in 0..l {
            let key = match object.get_dict_key(i as i32)? {
                Some(o) => o.as_name()?.to_owned(),
                None => return Err(Error::InvalidPdfDocument),
            };

            let value = object.get_dict_val(i as i32)?;
            if value.is_some() {
                map.insert(key, value.unwrap());
            }
        }

        Ok(Self { inner: map })
    }

    pub fn get(&self, key: &str) -> Option<&PdfObject> {
        self.inner.get(key)
    }
}

#[derive(Debug)]
pub struct Annotation {
    pub subtype: String,
    pub rect: Rect,
    pub contents: Option<String>,
    pub date: Option<NaiveDateTime>,
    pub author: Option<String>,
	inner: PdfObject
}

impl Annotation {
    pub fn from(object: PdfObject) -> Self {
        let mut subtype = "".to_owned();
        let mut rect = Rect::new(0.0, 0.0, 0.0, 0.0);
        let mut contents = None;
        let mut date = None;
        let mut author = None;
        let l = object.dict_len().unwrap();

        for i in 0..l {
            let key = object.get_dict_key(i as i32).unwrap().unwrap();
            let value = object.get_dict_val(i as i32).unwrap().unwrap();

            match key.as_name().unwrap() {
                "Subtype" => subtype = value.as_name().unwrap().to_owned(),
                "Rect" => {
                    let array = Array::from(&value).unwrap();
                    let array: Vec<f32> = array
                        .into_vec()
                        .into_iter()
                        .map(|o| o.as_float().unwrap())
                        .collect();

                    rect = Rect::new(array[0], array[1], array[2], array[3]);
                }
                "Contents" => contents = Some(value.as_string().unwrap().to_owned()),
                "Author" => author = Some(value.as_string().unwrap().to_owned()),
                "CreationDate" => {
                    let mut pdfdate = value.as_string().unwrap().to_owned();
                    pdfdate.truncate(16);
                    date = Some(NaiveDateTime::parse_from_str(&pdfdate, "D:%Y%m%d%H%M%S").unwrap());
                }
                _ => (),
            }
        }

        Self {
            subtype,
            rect,
            contents,
            date,
            author,
			inner: object
        }
    }

	pub fn set_visible(&mut self, is_visible: bool) -> Result<(), Error> {
		let l = self.inner.dict_len().unwrap();

        for i in 0..l {
            let key = self.inner.get_dict_key(i as i32).unwrap().unwrap();
            let value = self.inner.get_dict_val(i as i32).unwrap().unwrap();

			if let Ok("F") = key.as_name() {

				let mut x = value.as_int()?;
				//x = set_bit(0, x as u32, !is_visible) as i32;
				x = set_bit(1, x as u32, !is_visible) as i32;
				//x = set_bit(2, x as u32, is_visible) as i32;
				//x = set_bit(6, x as u32, !is_visible) as i32;

				let new_value = PdfObject::new_int(x).unwrap();
				self.inner.dict_put(key, new_value).unwrap();
				
				// let new_value = PdfObject::new_int(x).unwrap();
				// println!("{:?}", value.document().unwrap());
				// value.write_object(&new_value).unwrap();
				// new_value = document.add_object(&new_value).unwrap();
				// //new_value.create_object().unwrap();
				// new_value.write_object(&value).unwrap();
				// //value.write_object(&new_value).unwrap();
			}

		}

		Ok(())
	}

	pub fn is_visible(&self) -> Result<bool, Error> {
		let l = self.inner.dict_len().unwrap();

        for i in 0..l {
            let key = self.inner.get_dict_key(i as i32).unwrap().unwrap();
            let value = self.inner.get_dict_val(i as i32).unwrap().unwrap();

			if let Ok("F") = key.as_name() {

				let x = value.as_int()?;
				//x = set_bit(0, x as u32, !is_visible) as i32;
				return Ok(!get_bit(1, x as u32)) ;
				//x = set_bit(2, x as u32, is_visible) as i32;
				//x = set_bit(6, x as u32, !is_visible) as i32;

				
				// let new_value = PdfObject::new_int(x).unwrap();
				// println!("{:?}", value.document().unwrap());
				// value.write_object(&new_value).unwrap();
				// new_value = document.add_object(&new_value).unwrap();
				// //new_value.create_object().unwrap();
				// new_value.write_object(&value).unwrap();
				// //value.write_object(&new_value).unwrap();
			}

		}

		Err(Error::InvalidPdfDocument)
	}

	pub fn toggle_visible(&mut self) -> Result<(), Error> {
		self.set_visible(!self.is_visible()?)
	}

}

pub fn get_annotations(page: Page) -> Vec<Annotation> {

    let page = PdfPage::from(page);
    let page_object = page.object();
    let page_dict = Dict::from(page_object).unwrap();

    let annots = Array::from(page_dict.get("Annots").unwrap()).unwrap();

    let annots: Vec<Annotation> = annots
        .into_vec()
        .into_iter()
        .map(|o| Annotation::from(o))
        .collect();

    annots
}


#[cfg(test)]
mod tests {

	use mupdf::pdf::{PdfPage, PdfDocument};
	use super::*;

	const FILENAME: &str = "test.pdf";
	const PAGE_NR: i32 = 12;

	fn load_page() -> PdfPage {
		let doc = PdfDocument::open(FILENAME).unwrap();
		let page = doc.load_page(PAGE_NR).unwrap();
		PdfPage::from(page)
	}

	#[test]
	fn test_annotation() {
		let page = load_page();
		let page_object = page.object();
		let page_dict = Dict::from(page_object).unwrap();
		page_dict.get("Annots").unwrap();

		let annots = Array::from(page_dict.get("Annots").unwrap()).unwrap();
		let annots: Vec<Annotation> = annots
        .into_vec()
        .into_iter()
        .map(|o| Annotation::from(o))
        .collect();

		for mut a in annots {
			//println!("{:?}", a.inner.document().unwrap());
			a.set_visible(false).unwrap();
			a.set_visible(false).unwrap();
		}

	}
}