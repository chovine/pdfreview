# PDFReview

As I failed to find any FOSS PDF reader satisfying my particular needs (i.e. allowing decent processing of markdown comments), I decided to take a shot at writing my own.

The PDF rendering uses MuPDF and the GUI uses GTK4.


## Requirements

- MuPDF
- GTK4


## Build and Test

You need a working rust install (One-liner here [here](https://www.rust-lang.org/tools/install))

Simply run `cargo run --release`

Note that in debug mode, the rendering is very laggy.

## Status

This is very much a work in progress. Currently,
- filename and page number are hard coded
- document can be zoomed-in/out on pointer location using "p" and "m" keys
- comments' texts are displayed in a vertical list
- layout is not great
- lots of ugly stuff in order to quickly reach a POC

## References

1. [MuPDF Explored](https://ghostscript.com/~robin/mupdf_explored.pdf)
2. [PDF Specification v1.7](https://www.adobe.com/content/dam/acom/en/devnet/pdf/pdfs/PDF32000_2008.pdf)

## License

This project is licensed under the AGPLv3.0 license.
